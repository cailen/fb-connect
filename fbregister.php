<?php
// import wordpress database object
global $wpdb; 

// import options set via admin
define('FACEBOOK_APP_ID', get_option("fb_connect_api"));
define('FACEBOOK_SECRET', get_option("fb_connect_secret"));

// get correct DB prefix, usually wp_
$db_prefix = $wpdb->prefix;
$db_table = $db_prefix.'fbconnect';
$db_email = $db_prefix.'email';
$db_name = $db_prefix.'name';
$db_location = $db_prefix.'location';

function parse_signed_request($signed_request, $secret) {
  list($encoded_sig, $payload) = explode('.', $signed_request, 2); 

  // decode the data
  $sig = base64_url_decode($encoded_sig);
  $data = json_decode(base64_url_decode($payload), true);

  if (strtoupper($data['algorithm']) !== 'HMAC-SHA256') {
    //die('Unknown algorithm. Expected HMAC-SHA256');
    return null;
  }

  // check sig
  $expected_sig = hash_hmac('sha256', $payload, $secret, $raw = true);
  if ($sig !== $expected_sig) {
    //die('Bad Signed JSON signature!');
    return null;
  }

  return $data;
}

function base64_url_decode($input) {
    return base64_decode(strtr($input, '-_', '+/'));
}

if ($_REQUEST) {
  $response = parse_signed_request($_REQUEST['signed_request'], FACEBOOK_SECRET);
  $email = $reponse['registration']['email'];
  $name = $response['registration']['name'];
  $location = $response['registration']['location'];
  
  // check for existing user
  $r = $wpdb->query("select * from " .$db_table. "where " .$db_email_field. " = '$email'");
  if (mysql_num_rows($r) > 0)
	die(); // user already exists
  // otherwise, user is new - record info
  $sql = "insert into ".$db_table." ($db_name, $db_email, $db_location) values ('$name', '$email', '$location')";
  $wpdb->query($sql);
}								  
?>