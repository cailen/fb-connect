<?php
foreach($_GET as $k=>$v)
	$$k = $v;
	
$return_link = urlencode($return_link);

?>
<div id="fb_login">
	<h2><?=$title?></h2>
	<div id="fb_login_content"><?=$content?></div>
	<iframe src="https://www.facebook.com/plugins/registration?client_id=<?=$fb_client_id?>&redirect_uri=<?=$return_link?>&fields=name,location,email&action=Sign%20Up"
        scrolling="auto"
        frameborder="no"
        style="border:none"
        allowTransparency="true"
        width="100%"
        height="270">
	</iframe>
</div>

<style>
#fb_login {
	border-radius: 30px;
	background-color: #eee;
	border: 1px solid #333;
	padding: 50px;
	padding-top: 10px;
}

#fb_login h2 {
	font-family: sans-serif;
	font-size: smaller;
}

#fb_login #fb_login_content {
	border-radius: 3px;
	padding: 3px;
	background-color: #ddd;
	opacity: .9;
	margin-bottom: 20px;
}

#fb_login iframe {
	border-radius: 30px;
	
}
</style>